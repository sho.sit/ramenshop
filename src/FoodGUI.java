import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {


    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea t1;

    public void order(String food){
        int confimation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order"+ food +"?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confimation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "order for" + food + "received");
        };
    }
    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });


        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
    }
    
        public static void main (String[] args){
            JFrame frame = new JFrame("FoodGUI");
            frame.setContentPane(new FoodGUI().root);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        }
    }
