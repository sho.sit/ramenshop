import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RamenShop {
    private JPanel root;
    private JButton NegiButton;
    private JButton MisoRamenButton;
    private JButton ShoyuRamenButton;
    private JButton MenmaButton;
    private JButton ShioRamenButton;
    private JButton TamagoButton;
    private JTextField Total_Price;
    private JTextPane Selectall;
    private JButton checkOutButton;

    public String[] order(String food,int price_range) {
        String[] sizes = {"Large","Normal"};
        int c = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );
        int size = 1;

        if (c == 0) {
            if(food.contains("Ramen")){
            size = JOptionPane.showOptionDialog(
                    null,
                    "Choose the Size",
                    "Select Size",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    sizes,
                    sizes[1]
                    );
            }
            JOptionPane.showConfirmDialog(
                    null,
                    food + "("  + sizes[size] + ")" + " Selected.",
                    "Check select things.",
                    JOptionPane.OK_CANCEL_OPTION
            );
            int price = Orderprice(size, price_range);
return new String[]{sizes[size],String.valueOf(price)};
        }
        return null;
    }
    public int Orderprice(int s,int price_range){
        int[] range = {100,0};
        return price_range + range[s];
    }

    public int Result(int Total){
        int TotalPrice = Integer.parseInt(Total_Price.getText());
        return TotalPrice + Total;
    }


    public void ordertext(String food, int price_range) {
        String[] select = order(food, price_range);
        if(select == null) return;

        String size = select[0];
        int price = Integer.parseInt(select[1]);

        String currentText = Selectall.getText();
        Selectall.setText(currentText + food + "(" + size + ")\n");

        Total_Price.setText(String.valueOf(Result(price)));
    }

    public void Checkout() {
        int checkout = JOptionPane.showConfirmDialog(
                null,
                "Thank you. The total price is " + Result(0) + " yen.",
                "Order Received",
                JOptionPane.OK_CANCEL_OPTION
        );

        Selectall.setText(null); Total_Price.setText("0");
    }




    public RamenShop() {
        Total_Price.setText("0");

        NegiButton.setIcon(new javax.swing.ImageIcon(".\\src\\negi.jpg"));
        NegiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ordertext("Negi",50);
            }});
        MisoRamenButton.setIcon(new javax.swing.ImageIcon(".\\src\\miso.jpg"));
        MisoRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ordertext("MisoRamen",650);
            }});
        ShoyuRamenButton.setIcon(new javax.swing.ImageIcon(".\\src\\shoyu.jpg"));
        ShoyuRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ordertext("ShoyuRamen",700);
            }});
        MenmaButton.setIcon(new javax.swing.ImageIcon(".\\src\\menma.jpg"));
        MenmaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ordertext("Menma",80);
            }});
        ShioRamenButton.setIcon(new javax.swing.ImageIcon(".\\src\\shio.jpg"));
        ShioRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ordertext("ShioRamen",720);
            }});
        TamagoButton.setIcon(new javax.swing.ImageIcon(".\\src\\tamago.jpg"));
        TamagoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ordertext("Tamago",100);
            }});
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Checkout();
            }
        });
    }


    public static void main(String[] args){
        JFrame frame = new JFrame("RamenShop");
        frame.setContentPane(new RamenShop().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}


